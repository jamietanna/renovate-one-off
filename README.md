⚠⚠⚠⚠⚠⚠

This repo is deprecated. See https://www.jvt.me/posts/2022/12/12/renovate-one-off/ for an alternative way to do this.

⚠⚠⚠⚠⚠⚠

# Renovate One-Off Runner

A tool to perform one-off executions of [Renovate](https://docs.renovatebot.com/), along-side regular operation of Renovate.

This makes it possible to have Renovate pre-existing in your repositories/organisations, but be able to roll out arbitrary changes alongside Renovate, instead of conflicting with each other.

## Usage

Install the package with:

```sh
# to invoke with `npm exec renovate-one-off`
npm i @jamietanna/renovate-one-off
# to invoke with `renovate-one-off`
npm i -g @jamietanna/renovate-one-off
```

### `config.js`

To run `renovate-one-off`, you will need to have a `config.js` in the directory you're executing from.

For instance, let's say that in Go, there's a package called `github.com/<org>/grpc-models-go` and a Ruby gem called `grpc-models`, and in v2.0.0 there's a required update that all repositories must update to, for some arbitrary reason.

To do this, we could prepare the following `config.js`:

```javascript
module.exports = {
  // i.e.
  platform: "github",
  // if you pin to forks, for instance
  includeForks: true,

  // to only pin to certain package managers
  enabledManagers: ["ruby", "gomod"],

  packageRules: [
    // make sure that everything but what we want to raise updates for is disabled
    {
      "matchPackagePatterns": [".*"],
      "enabled": false
    },
    // for `grpc-models` the Ruby library, and github.com/<org>/grpc-models-go
    {
      "matchPackagePatterns": [".*grpc-models.*"],
      "enabled": true,
      "allowedVersions": ">=2.0.0"
    },
  ]
};
```

### A single repository

Running the following:

```sh
renovate-one-off --token $GITHUB_COM_TOKEN jamietanna/jamietanna
```

Will look through the repository, and raise any relevant changes expected for that repository, according to the `packageRules` defined.

### Autodiscovery (with a filter)

Running the following:

```sh
renovate-one-off --token $GITHUB_COM_TOKEN --autodiscover --autodiscover-filter 'jamietanna/*'
```

Will process the Repositories `jamietanna/jamietanna`, `jamietanna/disARM`, applying the relevant changes.

## License

As this is heavily modified code from Renovate itself, this project is licensed in the same was as Renovate - AGPL-3.0.
