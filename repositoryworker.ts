/*
 * Adapted from AGPL3 code in renovate's lib/workers/repository/index.ts
 */
import fs from 'fs-extra'
import { GlobalConfig } from 'renovate/dist/workers/repository/../../config/global'
import { applySecretsToConfig } from 'renovate/dist/workers/repository/../../config/secrets'
import type { RenovateConfig } from 'renovate/dist/workers/repository/../../config/types'
import { pkg } from 'renovate/dist/workers/repository/../../expose.cjs'
import { instrument } from 'renovate/dist/workers/repository/../../instrumentation'
import { logger, setMeta } from 'renovate/dist/workers/repository/../../logger'
import { removeDanglingContainers } from 'renovate/dist/workers/repository/../../util/exec/docker'
import { deleteLocalFile, privateCacheDir } from 'renovate/dist/workers/repository/../../util/fs'
import { isCloned } from 'renovate/dist/workers/repository/../../util/git'
import { clearDnsCache, printDnsStats } from 'renovate/dist/workers/repository/../../util/http/dns'
import * as queue from 'renovate/dist/workers/repository/../../util/http/queue'
import * as throttle from 'renovate/dist/workers/repository/../../util/http/throttle'
import * as schemaUtil from 'renovate/dist/workers/repository/../../util/schema'
import { addSplit, getSplits, splitInit } from 'renovate/dist/workers/repository/../../util/split'
import { setBranchCache } from 'renovate/dist/workers/repository/./cache'
import handleError from 'renovate/dist/workers/repository/./error'
import { finaliseRepo } from 'renovate/dist/workers/repository/./finalise'
import { initRepo } from 'renovate/dist/workers/repository/./init'
import { ensureOnboardingPr } from 'renovate/dist/workers/repository/./onboarding/pr'
import { extractDependencies, updateRepo } from 'renovate/dist/workers/repository/./process'
import { ProcessResult, processResult } from 'renovate/dist/workers/repository/./result'
import { printRequestStats } from 'renovate/dist/workers/repository/./stats'

// istanbul ignore next
export async function renovateRepository (
  repoConfig: RenovateConfig,
  canRetry = true
): Promise<ProcessResult | undefined> {
  splitInit()
  let config = GlobalConfig.set(
    applySecretsToConfig(repoConfig, undefined, false)
  )
  await removeDanglingContainers()
  setMeta({ repository: config.repository })
  logger.info({ renovateVersion: pkg.version }, 'Repository started')
  logger.trace({ config })
  let repoResult: ProcessResult | undefined
  queue.clear()
  throttle.clear()
  const localDir = GlobalConfig.get('localDir')!
  try {
    await fs.ensureDir(localDir)
    logger.debug('Using localDir: ' + localDir)
    config = await initRepo(config)
    addSplit('init')
    const { branches, branchList, packageFiles } = await instrument(
      'extract',
      async () => await extractDependencies(config)
    )
    if (
      GlobalConfig.get('dryRun') !== 'lookup' &&
      GlobalConfig.get('dryRun') !== 'extract'
    ) {
      await instrument('onboarding', async () =>
        await ensureOnboardingPr(config, packageFiles, branches)
      )
      addSplit('onboarding')

      const res = await instrument('update', async () =>
        await updateRepo(config, branches)
      )
      setMeta({ repository: config.repository })
      addSplit('update')
      await setBranchCache(branches)
      if (res === 'automerged') {
        if (canRetry) {
          logger.info('Renovating repository again after automerge result')
          const recursiveRes = await renovateRepository(repoConfig, false)
          return recursiveRes
        }
        logger.debug('Automerged but already retried once')
      }
      // Removed
      // else {
      //   await ensureDependencyDashboard(config, branches, packageFiles);
      // }
      // /Removed
      await finaliseRepo(config, branchList)
      // TODO #7154
      repoResult = processResult(config, res!)
    }
  } catch (err) /* istanbul ignore next */ {
    setMeta({ repository: config.repository })
    if (err instanceof Error) {
      const errorRes = await handleError(config, err)
      repoResult = processResult(config, errorRes)
    } else {
      console.error(err)
    }
  }
  if (localDir && !repoConfig.persistRepoData) {
    try {
      await deleteLocalFile('.')
    } catch (err) /* istanbul ignore if */ {
      logger.warn({ err }, 'localDir deletion error')
    }
  }
  try {
    await fs.remove(privateCacheDir())
  } catch (err) /* istanbul ignore if */ {
    logger.warn({ err }, 'privateCacheDir deletion error')
  }
  const splits = getSplits()
  logger.debug(splits, 'Repository timing splits (milliseconds)')
  printRequestStats()
  printDnsStats()
  clearDnsCache()
  schemaUtil.reportErrors()
  const cloned = isCloned()
  logger.info({ cloned, durationMs: splits.total }, 'Repository finished')
  return repoResult
}
